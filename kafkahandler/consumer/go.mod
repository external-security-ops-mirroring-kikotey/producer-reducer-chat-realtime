module main.go

go 1.16

require (
	github.com/go-redis/redis/v7 v7.4.1 // indirect
	github.com/twmb/franz-go v1.3.1
)
