package main

import (
	"context"
	"fmt"

        "github.com/go-redis/redis/v7"
	"github.com/twmb/franz-go/pkg/kgo"
)

func startConsuming(ctx context.Context, brokers []string, group, topic string) {
	clientRedis := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:6379", "localhost"),
		Password: "",
		DB:       0, // use default DB
	})

	_, err := clientRedis.Ping().Result()

	client, err := kgo.NewClient(
		kgo.SeedBrokers(brokers...),
		kgo.FetchIsolationLevel(kgo.ReadCommitted()), // only read messages that have been written as part of committed transactions
		kgo.ConsumerGroup(group),
		kgo.ConsumeTopics(topic),
	)
	if err != nil {
		fmt.Printf("error initializing Kafka consumer: %v\n", err)
		return
	}
	// The default blocking commit on leave will not run because the only
	// way to kill this program is to interrupt it, but, usually you will
	// close the client and wait for it to close before quitting. If you
	// want to perform an action on commit errors, you can use the
	// CommitCallback option.
	defer client.Close()
	userid := 0

consumerLoop:
	for {
		fetches := client.PollFetches(ctx)
		iter := fetches.RecordIter()
		userid++	
	
		for _, fetchErr := range fetches.Errors() {
			fmt.Printf("error consuming from topic: topic=%s, partition=%d, err=%v\n",
				fetchErr.Topic, fetchErr.Partition, fetchErr.Err)
			break consumerLoop
		}

		for !iter.Done() {
			record := iter.Next()
			fmt.Printf("consumed record from partition %d with message: %v \n", record.Partition, string(record.Value))
			newID, _ := clientRedis.XAdd(&redis.XAddArgs{
				Stream: fmt.Sprintf("room-%d", userid),
				Values: map[string]interface{}{
				"type": "valuetype",
				"data": string(record.Value),
			},
			}).Result()
			fmt.Printf("\n new redis id res: %v \n", newID)
		}
	}

	fmt.Println("consumer exited")
}
