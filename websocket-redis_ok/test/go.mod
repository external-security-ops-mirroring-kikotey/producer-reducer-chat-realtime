module test.go

go 1.16

require (
	github.com/garyburd/redigo v1.6.3
	github.com/gorilla/websocket v1.4.2
)
